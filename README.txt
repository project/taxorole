
Taxorole
URL: http://drupal.org/project/taxorole
------------------------
To install, place the entire taxorole folder into your modules directory.
Go to administer -> site building -> modules and enable the taxorole module.
If mass_contact or tac_lite modules are installed, additional features will 
be provided (optionally).

Now go to Administer > Site configuration > Taxorole settings. Select the 
vocabularies, whose terms will be used as roles.

You have the option to prefix the new role with the vocabular name and 
to use a user-specified delimiter.

If maxx_contact module is installed, taxorole will offer the option
to add a new category to mass_contact's categories with each new role
(useful when you want to send a mail to only a specific group of users
that have only access to a specific set of nodes).

Also, if tac_lite is installed you have the option to restrict access
to nodes tagged with the specified roles so that they will be visible
only by the respective role(s).

Finally, any new term will cause an automatic creation of a new role and 
(optionally) a new category in Mass Contact along with proper access
setttings in TAC lite.

Konstantinos Margaritis
CODEX
